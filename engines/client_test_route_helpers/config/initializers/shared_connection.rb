# frozen_string_literal: true
# Borrowed from https://gist.github.com/josevalim/470808
# Since transactions are per-connection only, and each request generates a new connection, this is required to keep
# transactions open across requests to the API

if defined? Rails::Server # don't share connection for rake tasks, console sessions, etc
  module ActiveRecord
    # rubocop:disable Style/ClassVars
    class Base
      mattr_accessor :shared_connection
      @@shared_connection = nil

      def self.connection
        @@shared_connection || ConnectionPool::Wrapper.new(size: 1) { retrieve_connection }
      end
    end
    # rubocop:enable Style/ClassVars
  end

  # Forces all threads to share the same connection.
  ActiveRecord::Base.shared_connection = ActiveRecord::Base.connection
end
