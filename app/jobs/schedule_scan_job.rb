# frozen_string_literal: true
class ScheduleScanJob
  @queue = :scan_scheduling

  class << self
    def perform(gid)
      @schedule = GlobalID::Locator.locate gid
      @schedule.scan_target.docker_commands.create!(containers: containers)
    rescue StandardError => exception
      # Airbrake doesn't wrap errors for plain Ruby classes, so catch the
      # notification here, notify Airbrake manually and re-raise the exception to Resque.
      # For more investigation, see where the Airbrake wrappers are loaded here:
      # https://github.com/airbrake/airbrake/tree/master/lib/airbrake/rails
      Airbrake.notify_sync exception
      raise exception
    end

    private

    def organization
      @schedule.organization
    end

    def containers
      (explicitly_enabled_containers + organization.required_containers.pluck(:name)).uniq
    end

    def machine_ids
      organization.machines.select(:id)
    end

    def explicitly_enabled_containers
      SecurityContainerConfig
        .includes(:security_container)
        .explicitly_enabled
        .where('(machine_id IN (?) OR organization_id = ?)', machine_ids, organization.id)
        .map { |config| config.security_container.name }
    end
  end
end
