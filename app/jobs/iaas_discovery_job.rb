# frozen_string_literal: true
require './lib/norad_fog_integration'

class IaasDiscoveryJob < ActiveJob::Base
  ActiveRecord::Base.logger = Resque.logger if defined?(Resque)
  queue_as :iaas_discovery

  rescue_from(StandardError) do |exception|
    Airbrake.notify_sync exception
    Resque.logger.error "Exception while discovering assets: #{exception.inspect}" if defined?(Resque)
    # XXX: The first argument passed to the perform method is the discovery object.
    discovery = @arguments.first
    ActiveRecord::Base.transaction do
      discovery.error_message = 'Failed to discover assets'
      discovery.fail!
    end
  end

  def perform(discovery, configuration)
    discovery.start!
    if configuration.openstack?
      NoradFogIntegration::OpenStackAssetDiscovery.new(configuration).start
    elsif configuration.aws?
      NoradFogIntegration::AwsAssetDiscovery.new(configuration).start
    end
    discovery.complete!
  end
end
