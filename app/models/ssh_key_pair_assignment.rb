# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: ssh_key_pair_assignments
#
#  id              :integer          not null, primary key
#  machine_id      :integer          not null
#  ssh_key_pair_id :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_ssh_key_pair_assignments_on_machine_id       (machine_id) UNIQUE
#  index_ssh_key_pair_assignments_on_ssh_key_pair_id  (ssh_key_pair_id)
#
# Foreign Keys
#
#  fk_rails_05e7648bab  (ssh_key_pair_id => ssh_key_pairs.id) ON DELETE => cascade
#  fk_rails_7c3731b704  (machine_id => machines.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class SshKeyPairAssignment < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # Validations
  validates :machine, presence: true
  validates :ssh_key_pair, presence: true
  validate :keypair_and_machine_in_same_organization

  # Associations
  belongs_to :machine
  belongs_to :ssh_key_pair

  include OrganizationErrorWatcher
  watch_for_organization_errors NoSshKeyPairAssignmentError, organization_method: :resolved_organization

  private

  def keypair_and_machine_in_same_organization
    return if machine&.organization_id == ssh_key_pair&.organization_id
    errors.add(:base, 'Key Pair and Machine must belong to the same organization')
  end

  def resolved_organization
    machine.organization
  end
end
