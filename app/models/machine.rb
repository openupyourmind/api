# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: machines
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  ip              :inet
#  fqdn            :string
#  description     :text
#  machine_status  :integer          default("pending"), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  name            :string           not null
#
# Indexes
#
#  index_machines_on_name_and_organization_id  (name,organization_id) UNIQUE
#  index_machines_on_organization_id           (organization_id)
#
# Foreign Keys
#
#  fk_rails_bd87ec17a7  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class Machine < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # Attribute Information
  enum machine_status: [:pending, :passing, :failing, :warning, :error]
  RANKED_ASSESSMENT_STATUSES = (Assessment::RANKED_ASSESSMENT_STATUSES.dup << :pending).freeze
  RFC1918_10_RANGE = '10.0.0.0/8'
  RFC1918_172_RANGE = '172.16.0.0/12'
  RFC1918_192_RANGE = '192.168.0.0/16'

  # Validations
  # NOTE: It is not ideal to check for uniqueness at the application level, but since we do not want
  # to treat "blank" as a unique value, we need to manually check for uniqueness
  validates :ip, presence: true, if: proc { |a| a.fqdn.blank? }
  validates :ip, uniqueness: { scope: :organization_id }, allow_blank: true
  validates :fqdn, presence: true, if: proc { |a| a.ip.blank? }
  validates :fqdn, uniqueness: { scope: :organization_id }, allow_blank: true
  validates :organization, presence: true
  validates :name, presence: true
  validates :name, uniqueness: { scope: :organization_id }
  validates :name, length: { maximum: 36 }
  validate :single_ip_specified

  # Associations
  belongs_to :organization, inverse_of: :machines
  has_many :docker_commands, inverse_of: :machine
  has_many :assessments, inverse_of: :machine
  has_many :security_container_configs, inverse_of: :machine
  has_many :scan_schedules, inverse_of: :machine, class_name: 'MachineScanSchedule'
  has_many :services, inverse_of: :machine
  has_many :ssh_services, inverse_of: :machine
  has_many :web_application_services, inverse_of: :machine
  has_many :service_discoveries, inverse_of: :machine
  has_one :ssh_key_pair_assignment, inverse_of: :machine
  has_one :ssh_key_pair, through: :ssh_key_pair_assignment
  has_many :result_ignore_rules, as: :ignore_scope, inverse_of: :ignore_scope, dependent: :destroy

  # Callback Declarations
  before_validation :generate_name, if: 'name.blank?'

  include OrganizationErrorWatcher
  watch_for_organization_errors UnreachableMachineError

  # Delegations
  delegate :to_s, to: :ip, prefix: true

  class << self
    # Return the set of machines with an enabled configuration for the given security container
    def with_configuration_for(container)
      joins(:security_container_configs)
        .where(
          security_container_configs: { security_container_id: container.id, enabled_outside_of_requirement: true }
        )
    end

    def rfc1918
      where('ip << ? OR ip << ? OR ip << ?', RFC1918_10_RANGE, RFC1918_172_RANGE, RFC1918_192_RANGE)
    end
  end

  def target_address
    fqdn.blank? ? ip_to_s : fqdn
  end

  def latest_assessment_stats
    assessments.latest_results_stats
  end

  def assessment_status
    Assessment.status_from_stats latest_assessment_stats
  end

  def ssh_key_values
    # FIXME: handle the SshConfig DNE case more gracefully
    {
      ssh_user: organization.configuration.use_relay_ssh_key ? '%{ssh_user}' : ssh_key_pair.try(:username),
      ssh_key: organization.configuration.use_relay_ssh_key ? '%{ssh_key}' : ssh_key_pair.try(:key)
    }
  end

  # Docker Commands created for an Organization also apply to the associated machines. This method
  # will return the more recent of any Docker Commands created for both the parent Organization and
  # the current instance of Machine
  def latest_command
    DockerCommand.where(organization: organization_id).or(DockerCommand.where(machine: id)).order(:created_at).last
  end

  def latest_results
    MachineWithLatestResults.new(self).latest_results
  end

  def rbac_parent
    organization
  end

  private

  def generate_name
    self.name = name.blank? ? SecureRandom.uuid : name
  end

  # The inet data type will prevent anything invalid from being added to this column, but as far as
  # the data type is concered, CIDR ranges are valid values. As of now, we don't want that, so we
  # should cast the ip to a string
  def single_ip_specified
    return if ip.blank? || min_ip_in_range == max_ip_in_range
    errors.add(:ip, 'address must be a valid, single IPv4 address')
  end

  def machine_ip_range
    ip.to_range
  end

  def min_ip_in_range
    machine_ip_range.min
  end

  def max_ip_in_range
    machine_ip_range.max
  end
end
