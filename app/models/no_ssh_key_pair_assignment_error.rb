# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_organization_errors_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class NoSshKeyPairAssignmentError < NoSshKeyPairError
  MESSAGE = 'You have one or more tests enabled that require authentication, but no SSH Key Pairs have been assigned.'

  class << self
    def check(org)
      required_ssh_key_pair_missing?(org) ? create_error(org) : remove_error(org)
    end

    def message
      MESSAGE
    end

    private

    def required_ssh_key_pair_missing?(org)
      !org_machines_have_ssh_key_pair_assignments?(org) && org_has_authenticated_tests_enabled?(org)
    end

    def org_machines_have_ssh_key_pair_assignments?(org)
      SshKeyPairAssignment.where(machine_id: org.machines).exists?
    end
  end

  def message
    MESSAGE
  end
end
