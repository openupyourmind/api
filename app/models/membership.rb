# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: memberships
#
#  id              :integer          not null, primary key
#  user_id         :integer          not null
#  organization_id :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_memberships_on_organization_id              (organization_id)
#  index_memberships_on_user_id                      (user_id)
#  index_memberships_on_user_id_and_organization_id  (user_id,organization_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_64267aab58  (organization_id => organizations.id) ON DELETE => cascade
#  fk_rails_99326fb65d  (user_id => users.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class Membership < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # Associations
  belongs_to :user
  belongs_to :organization

  def self.create_admin!(user, organization)
    membership = new(user_id: user.id, organization_id: organization.id)
    Membership.transaction do
      membership.save!
      user.add_role :organization_admin, organization
    end
    membership
  rescue StandardError
    membership
  end

  def self.create_reader!(user, organization)
    membership = new(user_id: user.id, organization_id: organization.id)
    Membership.transaction do
      membership.save!
      user.add_role :organization_reader, organization
    end
    membership
  rescue StandardError
    membership
  end

  def destroy_with_roles!
    Membership.transaction do
      determine_roles.each do |role|
        user.remove_role role, organization
      end
      destroy!
    end
  end

  def role
    user.roles.find_by(resource: organization)
  end

  private

  def determine_roles
    user.roles.each_with_object([]) do |role, a|
      a << role.name.to_sym if organization.id == role.resource_id && 'Organization' == role.resource_type
    end
  end
end
