# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: authentication_methods
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_authentication_methods_on_type     (type)
#  index_authentication_methods_on_user_id  (user_id) UNIQUE
#
# rubocop:enable Metrics/LineLength

class AuthenticationMethod < ApplicationRecord
  # Assocations
  belongs_to :user, inverse_of: :authentication_method

  class << self
    def local_auth?
      !reverse_proxy_auth?
    end

    def reverse_proxy_auth?
      sso_domain.present?
    end

    def find_or_create_user_for_authentication(params)
      if local_auth?
        User.confirmed_users.find_by email: params[:user]&.fetch(:email)
      elsif reverse_proxy_auth? # fall back to sso auth
        User.find_or_create_by_uid params[:id]
      end
    end

    def sso_domain
      ENV['SSO_DOMAIN']
    end
  end
end
