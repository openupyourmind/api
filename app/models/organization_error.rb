# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_organization_errors_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

class OrganizationError < ApplicationRecord
  belongs_to :organization, required: true

  class << self
    private

    def create_error(org)
      org.organization_errors.where(type: name).first_or_create
    end

    def remove_error(org)
      org.organization_errors.where(type: name).destroy_all
    end
  end
end
