# frozen_string_literal: true
module V1
  class OrganizationConfigurationsController < V1::ApplicationController
    before_action :set_config

    def show
      render json: @config
    end

    def update
      if @config.update(config_params)
        render json: @config
      else
        render json: { errors: @config.errors }
      end
    end

    private

    def set_config
      @config = OrganizationConfiguration.find(params[:id])
      authorize_action_for @config
    end

    def config_params
      params
        .require(:organization_configuration)
        .permit(:auto_approve_docker_relays, :use_relay_ssh_key)
    end
  end
end
