# frozen_string_literal: true
module V1
  class IaasDiscoveriesController < ApplicationController
    before_action :set_organization, only: [:index, :create]

    def index
      authorize_action_for IaasDiscovery, in: @org
      render json: @org.iaas_discoveries.limit(10)
    end

    def show
      @iaas_discovery = IaasDiscovery.find(params[:id])
      authorize_action_for @iaas_discovery
      render json: @iaas_discovery
    end

    def create
      authorize_action_for IaasDiscovery, in: @org
      discovery = IaasDiscovery.new(iaas_configuration: @org.iaas_configuration)
      if discovery.save
        render json: discovery
      else
        render_errors_for(discovery)
      end
    end

    private

    def set_organization
      @org = Organization.find(params[:organization_id])
    end
  end
end
