# frozen_string_literal: true
module V1
  class ProvisionsController < ApplicationController
    before_action :set_requirement, only: [:create]
    before_action :set_provision, only: [:destroy]

    def create
      authorize_action_for Provision, in: @requirement
      provision = @requirement.provisions.build(creation_params)
      if provision.save
        render json: provision
      else
        render_errors_for(provision)
      end
    end

    def destroy
      authorize_action_for @provision
      @provision.destroy!
      head :no_content
    end

    private

    def set_requirement
      @requirement = Requirement.find(params[:requirement_id])
    end

    def set_provision
      @provision = Provision.find(params[:id])
    end

    def creation_params
      params.require(:provision).permit(:security_container_id)
    end
  end
end
