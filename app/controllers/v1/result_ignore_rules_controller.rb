# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: result_ignore_rules
#
#  id                :integer          not null, primary key
#  ignore_scope_id   :integer          not null
#  ignore_scope_type :string           not null
#  signature         :string           not null
#  comment           :text
#  created_by        :string           not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_result_ignore_rules_on_i_s_type_and_i_s_id  (ignore_scope_type,ignore_scope_id)
#  index_result_ignore_rules_on_signature            (signature)
#
# rubocop:enable Metrics/LineLength

module V1
  class ResultIgnoreRulesController < ApplicationController
    include BelongsToPolyParent
    before_action :set_record, only: [:show, :destroy]

    private

    def creation_params
      params.require(:result_ignore_rule).permit(:signature, :comment).merge(created_by: current_user.uid)
    end
  end
end
