# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: ssh_key_pairs
#
#  id                 :integer          not null, primary key
#  name               :string           not null
#  description        :text
#  username_encrypted :string
#  key_encrypted      :string
#  organization_id    :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_ssh_key_pairs_on_name_and_organization_id  (name,organization_id) UNIQUE
#  index_ssh_key_pairs_on_organization_id           (organization_id)
#
# Foreign Keys
#
#  fk_rails_3811d4539d  (organization_id => organizations.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

module V1
  class SshKeyPairsController < ApplicationController
    before_action :set_organization, only: [:index, :create]
    before_action :set_ssh_key_pair, only: [:show, :update, :destroy]

    def index
      render json: @org.ssh_key_pairs
    end

    def show
      render json: @ssh_key_pair
    end

    def create
      key_pair = @org.ssh_key_pairs.build(ssh_key_pair_create_params)
      if key_pair.save
        render json: key_pair
      else
        render_errors_for(key_pair)
      end
    end

    def update
      if @ssh_key_pair.update(ssh_key_pair_update_params)
        render json: @ssh_key_pair
      else
        render_errors_for(@ssh_key_pair)
      end
    end

    def destroy
      @ssh_key_pair.destroy
      head :no_content
    end

    private

    def set_ssh_key_pair
      @ssh_key_pair = SshKeyPair.find(params[:id])
      authorize_action_for @ssh_key_pair
    end

    def set_organization
      @org = Organization.find(params[:organization_id])
      authorize_action_for SshKeyPair, in: @org
    end

    def ssh_key_pair_create_params
      params.require(:ssh_key_pair).permit(:name, :description, :username, :key)
    end

    def ssh_key_pair_update_params
      params.require(:ssh_key_pair).permit(:name, :description)
    end
  end
end
