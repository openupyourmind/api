# frozen_string_literal: true
module DockerRelayStateMachine
  extend ActiveSupport::Concern

  included do
    include AASM

    enum state: { offline: 0, online: 1 }

    aasm column: :state, enum: true do
      state :online, initial: true
      state :offline

      after_all_transitions :notify_organization_admins

      event :go_offline do
        transitions from: :online, to: :offline
      end

      event :go_online do
        transitions from: :offline, to: :online
      end
    end

    private

    def notify_organization_admins
      if aasm.to_state == :online
        RelayMailer.online_notification(self).deliver_later
      else
        RelayMailer.offline_notification(self).deliver_later
      end
    end
  end
end
