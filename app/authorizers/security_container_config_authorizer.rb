# frozen_string_literal: true
class SecurityContainerConfigAuthorizer < BelongsToPolyParentAuthorizer
  private

  def poly_assoc_name
    :configurable
  end
end
