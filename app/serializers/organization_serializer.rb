# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: organizations
#
#  id         :integer          not null, primary key
#  uid        :string           not null
#  slug       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_organizations_on_slug  (slug) UNIQUE
#  index_organizations_on_uid   (uid) UNIQUE
#
# rubocop:enable Metrics/LineLength

class OrganizationSerializer < ActiveModel::Serializer
  attributes :id, :uid, :slug, :token, :machine_count, :machine_assessment_summary
  has_one :configuration
  has_many :organization_errors

  def token
    object.token if user_is_org_admin?
  end

  def machine_count
    object.machines.count
  end

  def machine_assessment_summary
    return {} unless instance_options[:include_assessment_summary]
    object.machine_assessment_summary
  end

  private

  def user_is_org_admin?
    current_user && current_user.has_role?(:organization_admin, object)
  end

  def current_user
    Thread.current['current_user']
  end
end
