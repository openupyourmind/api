# frozen_string_literal: true
class ApplicationMailer < ActionMailer::Base
  SUBJECT_PREFIX = '[Norad Notification]'
  default from: 'no-reply@norad.cisco.com'

  helper do
    def greeting_name
      @user.firstname && " #{@user.firstname}"
    end

    def ui_url(url)
      uri = URI.parse(url)
      uri.path.sub!(%r{^/v1}, '')
      uri.to_s
    end

    def duration_in_words(dc)
      distance_of_time_in_words(dc.started_at || dc.created_at, dc.finished_at || Time.now.utc)
    end

    def scan_url(dc)
      url =
        if dc.machine.present?
          "#{v1_machine_url(dc.machine)}/results?scan_id=#{dc.id}"
        else
          v1_organization_url(dc.organization)
        end
      ui_url(url)
    end
  end
end
