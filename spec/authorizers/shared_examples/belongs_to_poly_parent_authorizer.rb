# frozen_string_literal: true
require 'rails_helper'

RSpec.shared_examples 'belongs to poly parent authorizer' do
  let(:user) { build_stubbed :user }
  let(:admin_role) { :organization_admin }
  let(:reader_role) { :organization_reader }

  shared_examples "CRUD Rules for Creating #{described_class}" do
    describe 'the create policy' do
      it 'allows an admin on the associated organization to create a new policy' do
        allow(user).to receive(:has_role?).with(admin_role, organization).and_return(true)
        allow(user).to receive(:has_role?).with(reader_role, organization).and_return(false)
        expect(associated_klass.authorizer).to be_creatable_by(user, in: organization)
      end

      it 'prevents a reader on the associated organization from creating a new policy' do
        allow(user).to receive(:has_role?).with(admin_role, organization).and_return(false)
        allow(user).to receive(:has_role?).with(reader_role, organization).and_return(true)
        expect(associated_klass.authorizer).to_not be_creatable_by(user, in: organization)
      end

      it 'prevents a user without roles from creating a new policy' do
        allow(user).to receive(:has_role?).with(admin_role, organization).and_return(false)
        allow(user).to receive(:has_role?).with(reader_role, organization).and_return(false)
        expect(associated_klass.authorizer).to_not be_creatable_by(user, in: organization)
      end
    end

    describe 'the read policy' do
      it 'allows an admin on the associated organization to read policies' do
        allow(user).to receive(:has_role?).with(admin_role, organization).and_return(true)
        allow(user).to receive(:has_role?).with(reader_role, organization).and_return(false)
        expect(associated_klass.authorizer).to be_readable_by(user, in: organization)
      end

      it 'allows a reader on the associated organization to read policies' do
        allow(user).to receive(:has_role?).with(admin_role, organization).and_return(false)
        allow(user).to receive(:has_role?).with(reader_role, organization).and_return(true)
        expect(associated_klass.authorizer).to be_readable_by(user, in: organization)
      end

      it 'prevents a user without roles from reading policies' do
        allow(user).to receive(:has_role?).with(admin_role, organization).and_return(false)
        allow(user).to receive(:has_role?).with(reader_role, organization).and_return(false)
        expect(associated_klass.authorizer).to_not be_readable_by(user, in: organization)
      end
    end

    describe 'the update policy' do
      it 'allows an admin on the associated organization to update a policy' do
        allow(user).to receive(:has_role?).with(admin_role, organization).and_return(true)
        expect(rule.authorizer).to be_updatable_by(user)
      end

      it 'prevents a reader on the associated organization from updating a policy' do
        allow(user).to receive(:has_role?).with(admin_role, organization).and_return(false)
        expect(rule.authorizer).to_not be_updatable_by(user)
      end

      it 'prevents a user without roles from updating a policy' do
        allow(user).to receive(:has_role?).with(admin_role, organization).and_return(false)
        expect(rule.authorizer).to_not be_updatable_by(user)
      end
    end

    describe 'the destroy policy' do
      it 'allows an admin on the associated organization to delete a policy' do
        allow(user).to receive(:has_role?).with(admin_role, organization).and_return(true)
        expect(rule.authorizer).to be_deletable_by(user)
      end

      it 'prevents a reader on the associated organization from deleting a policy' do
        allow(user).to receive(:has_role?).with(admin_role, organization).and_return(false)
        expect(rule.authorizer).to_not be_deletable_by(user)
      end

      it 'prevents a user without roles from deleting a policy' do
        allow(user).to receive(:has_role?).with(admin_role, organization).and_return(false)
        expect(rule.authorizer).to_not be_deletable_by(user)
      end
    end
  end

  context 'for rules created for machines' do
    include_examples "CRUD Rules for Creating #{described_class}" do
      let(:machine) { create :machine }
      let(:organization) { machine.organization }
      let(:rule) { build_stubbed factory_name, poly_assoc_name => machine }
    end
  end

  context 'for rules created for organizations' do
    include_examples "CRUD Rules for Creating #{described_class}" do
      let(:organization) { create :organization }
      let(:rule) { build_stubbed factory_name, poly_assoc_name => organization }
    end
  end
end
