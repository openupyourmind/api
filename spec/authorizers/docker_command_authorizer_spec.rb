# frozen_string_literal: true
require 'rails_helper'
require 'authorizers/shared_examples/belongs_to_poly_parent_authorizer'

describe DockerCommandAuthorizer, type: :authorizer do
  include_examples 'belongs to poly parent authorizer' do
    let(:associated_klass) { DockerCommand }
    let(:poly_assoc_name) { 'commandable' }
    let(:factory_name) { :docker_command }
  end
end
