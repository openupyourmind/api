# frozen_string_literal: true
require 'rails_helper'

describe AssessmentAuthorizer, type: :authorizer do
  before :each do
    @user = create :user
    @org = create :organization
    machine = create :machine, organization: @org
    @wba = create :white_box_assessment, machine: machine
    @bba = create :black_box_assessment, machine: machine
  end

  context 'when authorizing at the instance level' do
    context 'a reader' do
      before :each do
        @user.add_role :organization_reader, @org
      end

      it 'can read' do
        expect(@wba.authorizer).to be_readable_by(@user)
      end
    end

    context 'an admin' do
      before :each do
        @user.add_role :organization_admin, @org
      end

      it 'can read' do
        expect(@wba.authorizer).to be_readable_by(@user)
        expect(@bba.authorizer).to be_readable_by(@user)
      end
    end
  end

  context 'when authorizing at the class level' do
    context 'a reader' do
      before :each do
        @user.add_role :organization_reader, @org
      end

      it 'can read' do
        expect(WhiteBoxAssessment.authorizer).to be_readable_by(@user, in: @org)
        expect(BlackBoxAssessment.authorizer).to be_readable_by(@user, in: @org)
      end
    end

    context 'an admin' do
      before :each do
        @user.add_role :organization_admin, @org
      end

      it 'can read' do
        expect(WhiteBoxAssessment.authorizer).to be_readable_by(@user, in: @org)
        expect(BlackBoxAssessment.authorizer).to be_readable_by(@user, in: @org)
      end
    end
  end
end
