# frozen_string_literal: true
require 'rails_helper'

describe UserAuthorizer, type: :authorizer do
  before :each do
    @user1 = create :user
    @user2 = create :user
  end

  context 'User action' do
    it 'try to update' do
      expect(@user1.authorizer).to be_updatable_by(@user1)
      expect(@user1.authorizer).to_not be_updatable_by(@user2)
    end

    it 'try to read' do
      expect(@user1.authorizer).to be_readable_by(@user1)
      expect(@user1.authorizer).to_not be_readable_by(@user2)
    end
  end
end
