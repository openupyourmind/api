# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: docker_commands
#
#  id                    :integer          not null, primary key
#  error_details         :text
#  containers            :json
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  machine_id            :integer
#  organization_id       :integer
#  state                 :integer          default("pending"), not null
#  assessments_in_flight :integer          default(0)
#  started_at            :datetime
#  finished_at           :datetime
#
# Indexes
#
#  index_docker_commands_on_machine_id       (machine_id)
#  index_docker_commands_on_organization_id  (organization_id)
#
# Foreign Keys
#
#  fk_rails_0545a1ce1d  (organization_id => organizations.id) ON DELETE => cascade
#  fk_rails_b85f190c31  (machine_id => machines.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'

RSpec.describe DockerCommand, type: :model do
  context 'when validating' do
    it 'requires a machine to be present if an organization is not' do
      dc = DockerCommand.new
      dc.valid?
      expect(dc.errors.messages[:machine]).to include "can't be blank"
      dc.organization = create :organization
      dc.valid?
      expect(dc.errors.messages[:machine]).to eq []
    end

    it 'requires an organization to be present if a machine is not' do
      dc = DockerCommand.new
      dc.valid?
      expect(dc.errors.messages[:organization]).to include "can't be blank"
      dc.machine = create :machine
      dc.valid?
      expect(dc.errors.messages[:organization]).to eq []
    end

    context 'the container attribute' do
      before :each do
        @dc = DockerCommand.new
        allow(@dc).to receive(:ensure_containers_attribute_is_a_flat_array).and_return(true)
      end

      it 'should only allow an array' do
        @dc.containers = 'a string'
        expect(@dc.valid?).to be false
        expect(@dc.errors.messages[:containers]).to include 'must be an array'
        @dc.containers = []
        @dc.valid?
        expect(@dc.errors.messages[:containers]).to_not include 'must be an array'
      end

      it 'should only allow a non empty list' do
        @dc.containers = []
        expect(@dc.valid?).to be false
        expect(@dc.errors.messages[:containers]).to include 'list must include at least one container name'
        @dc.containers = ['container1']
        @dc.valid?
        expect(@dc.errors.messages[:containers]).to_not include 'list must include at least one container name'
      end

      it 'should only allow strings in the list' do
        @dc.containers = ['string', 1, {}]
        expect(@dc.valid?).to be false
        expect(@dc.errors.messages[:containers]).to include 'must only include strings'
        @dc.containers = %w(string1 string2)
        @dc.valid?
        expect(@dc.errors.messages[:containers]).to_not include 'must only include strings'
      end

      it 'should only allow container names that exist in the system' do
        @dc.containers = ['madeup']
        expect(@dc.valid?).to be false
        expect(@dc.errors.messages[:containers]).to include 'one or more requested containers is invalid'
        cname = create(:security_container).name
        @dc.containers << cname
        @dc.valid?
        expect(@dc.errors.messages[:containers]).to include 'one or more requested containers is invalid'
        @dc.containers = [cname]
        @dc.valid?
        messages = @dc.errors.messages[:containers].nil? ? [] : @dc.errors.messages[:containers]
        expect(messages).to_not include 'one or more requested containers is invalid'
      end
    end
  end

  context 'when executing callbacks' do
    context 'before destroy' do
      before :each do
        @dc = create :docker_command,
                     commandable: create(:organization),
                     containers: [create(:security_container).name]
      end

      it 'should allow removal when scan is complete' do
        @dc.complete!
        expect { @dc.destroy }.to change(DockerCommand, :count).by(-1)
        expect { @dc.reload }.to raise_exception ActiveRecord::RecordNotFound
      end

      it 'should allow removal when scan is canceled' do
        @dc.cancel!
        expect { @dc.destroy }.to change(DockerCommand, :count).by(-1)
        expect { @dc.reload }.to raise_exception ActiveRecord::RecordNotFound
      end

      it 'should prevent removal when scan is pending' do
        expect(@dc.pending?).to be true
        expect { @dc.destroy }.not_to change(DockerCommand, :count)
      end

      it 'should prevent removal when scan is in flight' do
        allow(@dc).to receive(:assessments).and_return(['an assessment'])
        @dc.complete!
        expect(@dc.assessments_created?).to be true
        expect(@dc.finished_at).to be_blank
        expect { @dc.destroy }.not_to change(DockerCommand, :count)
      end
    end

    context 'before validation' do
      it 'should set the containers attribute to an empty array if it is some other type' do
        dc = DockerCommand.new containers: 'a string'
        expect(dc.containers.is_a?(Array)).to be false
        dc.valid?
        expect(dc.containers.is_a?(Array)).to be true
      end

      it 'should flatten the containers array if it is nested' do
        nested_array = ['a', ['b']]
        dc = DockerCommand.new containers: nested_array
        expect(dc.containers).to eq nested_array
        dc.valid?
        expect(dc.containers).to eq nested_array.flatten
      end
    end
  end

  context 'when maintaining associations' do
    it { should belong_to(:machine) }
    it { should belong_to(:organization) }
    it { should have_many(:assessments) }
  end

  context 'when running assessments' do
    let(:machine) { build(:machine) }

    before :each do
      @started_at = 1.day.ago
      @finished_at = Time.now.utc
    end

    context 'when scanning an org' do
      let(:org) { create(:organization, machines: [create(:machine)]) }

      before :each do
        @dc = DockerCommand.new(
          containers: ['a container'],
          organization: org,
          started_at: @started_at,
          finished_at: @finished_at
        )
      end

      it 'returns the correct machine count' do
        expect(@dc.machine_count).to eq 1
      end

      it 'increments and decrements assessments_in_flight' do
        @dc.increment :assessments_in_flight
        expect(@dc.assessments_in_flight).to eq 1
        @dc.decrement :assessments_in_flight
        expect(@dc.assessments_in_flight).to eq 0
      end
    end

    context 'when scanning a machine' do
      before :each do
        @dc = DockerCommand.new(
          containers: ['a container'],
          machine: machine,
          started_at: @started_at,
          finished_at: @finished_at
        )
      end

      it 'returns the correct machine count' do
        expect(@dc.machine_count).to eq 1
      end

      it 'increments and decrements assessments_in_flight' do
        @dc.increment :assessments_in_flight
        expect(@dc.assessments_in_flight).to eq 1
        @dc.decrement :assessments_in_flight
        expect(@dc.assessments_in_flight).to eq 0
      end
    end
  end
end
