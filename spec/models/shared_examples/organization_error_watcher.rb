# frozen_string_literal: true
RSpec.shared_examples 'an Organization Error Watcher' do
  it 'adds the ::watch_for_organization_errors class method' do
    expect(described_class.respond_to?(:watch_for_organization_errors)).to eq(true)
  end
end
