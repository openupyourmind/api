# frozen_string_literal: true
RSpec.shared_examples 'an Organization Error class' do
  describe 'the ::message class method' do
    it 'returns the MESSAGE constant' do
      expect(described_class.message).to eq(described_class::MESSAGE)
    end
  end

  describe 'the #message instance method' do
    it 'returns the MESSAGE constant' do
      an_instance = described_class.new
      expect(an_instance.message).to eq(described_class::MESSAGE)
    end
  end
end
