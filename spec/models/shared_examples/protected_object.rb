# frozen_string_literal: true
RSpec.shared_examples 'a Protected Object' do
  it 'defines .creatable_by?' do
    expect(described_class).to respond_to(:creatable_by?)
  end

  it 'defines .readable_by?' do
    expect(described_class).to respond_to(:readable_by?)
  end

  it 'defines .updatable_by?' do
    expect(described_class).to respond_to(:updatable_by?)
  end

  it 'defines .delatable_by?' do
    expect(described_class).to respond_to(:deletable_by?)
  end

  it 'defines #creatable_by?' do
    expect(subject).to respond_to(:creatable_by?)
  end

  it 'defines #readable_by?' do
    expect(subject).to respond_to(:readable_by?)
  end

  it 'defines #updatable_by?' do
    expect(subject).to respond_to(:updatable_by?)
  end

  it 'defines #delatable_by?' do
    expect(subject).to respond_to(:deletable_by?)
  end
end
