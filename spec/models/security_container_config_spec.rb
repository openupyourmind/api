# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: security_container_configs
#
#  id                             :integer          not null, primary key
#  security_container_id          :integer
#  enabled_outside_of_requirement :boolean          default(FALSE), not null
#  machine_id                     :integer
#  organization_id                :integer
#  values_encrypted               :string
#
# Indexes
#
#  index_security_container_configs_on_machine_id             (machine_id)
#  index_security_container_configs_on_organization_id        (organization_id)
#  index_security_container_configs_on_security_container_id  (security_container_id)
#
# Foreign Keys
#
#  fk_rails_191d669b7b  (machine_id => machines.id) ON DELETE => cascade
#  fk_rails_7f035feecf  (organization_id => organizations.id) ON DELETE => cascade
#  fk_rails_a879277af4  (security_container_id => security_containers.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'
require 'models/shared_examples/organization_error_watcher'

RSpec.describe SecurityContainerConfig, type: :model do
  context 'when enforcing attribute restrictions' do
    it { should have_readonly_attribute(:security_container_id) }
    it { should have_readonly_attribute(:machine_id) }
    it { should have_readonly_attribute(:organization_id) }
  end

  context 'when validating' do
    it 'requires a machine to be present if an organization is not' do
      sc = SecurityContainerConfig.new
      sc.valid?
      expect(sc.errors.messages[:machine]).to include "can't be blank"
      sc.organization = create :organization
      sc.valid?
      expect(sc.errors.messages[:machine]).to eq([])
    end

    it 'requires an organization to be present if a machine is not' do
      sc = SecurityContainerConfig.new
      sc.valid?
      expect(sc.errors.messages[:organization]).to include "can't be blank"
      sc.machine = create :machine
      sc.valid?
      expect(sc.errors.messages[:organization]).to eq([])
    end
    it { should validate_presence_of(:security_container) }

    it 'should verify that only arguments expected by the container are provided' do
      sc = create :security_container, prog_args: '-p %{port} %{target}', default_config: { port: 443 }
      config = build :security_container_config, security_container: sc
      expect(config.valid?).to be true
      config.values['extra_junk'] = 'foo'
      expect(config.valid?).to be false
      expect(config.errors.messages[:values]).to include 'must contain exact arguments'
      config.values.delete('extra_junk')
      config.values.delete('port')
      expect(config.valid?).to be false
      expect(config.errors.messages[:values]).to include 'must contain exact arguments'
      config.values['port'] = '443'
      expect(config.valid?).to be true
    end

    it 'should verify that the hash values are nonempty' do
      sc = create :security_container, prog_args: '-p %{port} %{target}', default_config: { port: 443 }
      config = build :security_container_config, security_container: sc
      config.values['port'] = ''
      expect(config.valid?).to be false
      expect(config.errors.messages[:values]).to include 'can only contain nonempty string values'
      config.values['port'] = '443'
      expect(config.valid?).to be true
    end
  end

  context 'when maintaining associations' do
    it { should belong_to(:machine) }
    it { should belong_to(:organization) }
    it { should belong_to(:security_container) }
  end

  context 'when executing callbacks' do
    context 'before validation' do
      it 'converts all values to strings' do
        sc = create :security_container, prog_args: '-p %{port} %{target}', default_config: { port: 443 }
        config = build :security_container_config, security_container: sc
        expect(config.values['port'].is_a?(Integer)).to be true
        config.valid? # trigger validations and callbacks
        expect(config.values['port'].is_a?(String)).to be true
      end
    end

    context 'after saving' do
      it 'checks for SSH Key Pair Errors after creation' do
        org = create :organization
        config = build :security_container_config, organization: org
        expect(NoSshKeyPairError).to receive(:check).with(org)
        config.save!
      end

      it 'checks for SSH Key Pair Errors after updating' do
        org = create :organization
        config = create :security_container_config, organization: org, machine: nil
        expect(NoSshKeyPairError).to receive(:check).with(config.organization)
        config.enabled_outside_of_requirement = false
        config.save!
      end

      it 'checks for SSH Key Pair Errors after creating a machine level config' do
        config = build :security_container_config, machine: create(:machine)
        expect(NoSshKeyPairError).to receive(:check).with(config.machine.organization)
        config.save!
      end

      it 'checks for SSH Key Pair Assignment Errors after creation' do
        org = create :organization
        config = build :security_container_config, organization: org
        expect(NoSshKeyPairAssignmentError).to receive(:check).with(org)
        config.save!
      end

      it 'checks for SSH Key Pair Assignment Errors after updating' do
        org = create :organization
        config = create :security_container_config, organization: org, machine: nil
        expect(NoSshKeyPairAssignmentError).to receive(:check).with(config.organization)
        config.enabled_outside_of_requirement = false
        config.save!
      end

      it 'checks for SSH Key Pair Assignment Errors after creating a machine level config' do
        config = build :security_container_config, machine: create(:machine)
        expect(NoSshKeyPairAssignmentError).to receive(:check).with(config.machine.organization)
        config.save!
      end
    end

    context 'after destroying' do
      it 'checks for the SSH Key Pair Error' do
        org = create :organization
        config = create :security_container_config, organization: org, machine: nil
        expect(NoSshKeyPairError).to receive(:check).with(config.organization)
        config.destroy!
      end
    end
  end

  context 'when receiving messages for convenience methods' do
    it 'returns the config values with symbolized keys' do
      config = SecurityContainerConfig.new values: { 'foo' => 'bar' }
      expect(config.values['foo']).to_not be nil
      expect(config.values[:foo]).to be nil
      expect(config.args_hash['foo']).to be nil
      expect(config.args_hash[:foo]).to_not be nil
    end

    it 'adds a temporary placeholder for spaces' do
      config = SecurityContainerConfig.new values: { 'foo' => 'bar baz' }
      expect(config.values['foo']).to eq 'bar baz'
      expect(config.args_hash[:foo]).to eq "bar#{SecurityContainer::SPACE_PLACEHOLDER}baz"
    end
  end

  context 'when watching for organization errors' do
    it_behaves_like 'an Organization Error Watcher'
  end
end
