# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: organizations
#
#  id         :integer          not null, primary key
#  uid        :string           not null
#  slug       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_organizations_on_slug  (slug) UNIQUE
#  index_organizations_on_uid   (uid) UNIQUE
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'
require 'models/shared_examples/ignore_scope'
require 'models/shared_examples/protected_object'

RSpec.describe Organization, type: :model do
  context 'organization slug collision', with_after_commit: true do
    it 'when UID is similar enough to generate the same slug' do
      first_org = build :organization, uid: 'My Org'
      second_org = build :organization, uid: 'My-Org'
      first_org.save!
      expect { second_org.save! }.not_to raise_error
      expect(first_org.slug).to_not eq(second_org.slug)
      expect(second_org.slug =~ /\A[A-z][A-z0-9-]+\z/).not_to be(nil)
    end
  end

  context 'when validating' do
    it { should validate_presence_of :uid }
    it { should allow_value('Acme Inc').for(:uid) }
    it { should allow_value('acme Inc').for(:uid) }
    it { should_not allow_value('1 Acme Inc').for(:uid) }
    it { should_not allow_value('Acme Inc.').for(:uid) }
    it { should allow_value('acme_inc').for(:uid) }
    it { should allow_value('cAMMeL CaSE 4u cO').for(:uid) }
    it { should_not allow_value('Acme Org Default').for(:uid) }
    it { should_not allow_value('acme org default').for(:uid) }
  end

  context 'default and org UID namespace collisions' do
    it 'UIDs that would resolve to the same without reserved word' do
      organization = build :organization, uid: 'johnchambers-org'
      organization.save!
      user = build :user, uid: 'johnchambers'
      expect { user.save! }.to_not raise_error
      expect(user.organizations[0].uid).to eq("johnchambers-org-#{Organization::DEFAULT_SUFFIX}")
    end

    it 'a user and organization use the same UID and succeed' do
      organization = build :organization, uid: 'johnchambers'
      organization.save!
      user = build :user, uid: 'johnchambers'
      expect { user.save! }.to_not raise_error
      expect(user.organizations[0].uid).to eq("johnchambers-org-#{Organization::DEFAULT_SUFFIX}")
    end

    it 'organization trying to use reserved word and failing' do
      organization = build :organization, uid: "johnchambers org #{Organization::DEFAULT_SUFFIX}"
      expect { organization.save! }.to raise_error ActiveRecord::RecordInvalid
    end
  end

  context 'when executing callbacks' do
    it 'populates default notification channels' do
      org = build :organization
      expect(org.notification_channels).to be_empty
      org.save!
      expect(org.notification_enabled?('scan_complete')).to be true
    end

    it 'generates a slug based on the uid' do
      org = build :organization
      expect(org.slug).to be nil
      org.save!
      expect(org.slug).to eq org.uid.parameterize
    end

    it 'builds an organization token' do
      org = create :organization
      expect(org.organization_token).to_not be nil
    end

    it 'builds a configuration' do
      org = build :organization
      expect(org).to receive(:build_configuration).once
      org.save!
      expect(org.configuration).to_not be nil
    end
  end

  context 'when maintaining associations' do
    it { should have_many(:users).through(:memberships) }
    it { should have_many(:machines) }
    it { should have_one(:organization_token) }
    it { should have_one(:configuration) }
    it { should have_one(:iaas_configuration) }
    it { should have_many(:docker_relays) }
    it { should have_many(:ssh_key_pairs) }
    it { should have_many(:result_ignore_rules).dependent(:destroy) }

    it 'provides a list of containers enabled via requirements' do
      org = create :organization
      rg = create :requirement_group
      req = build :requirement
      sec_con1 = create :security_container
      create :security_container
      rg.requirements << req
      req.security_containers << sec_con1
      org.requirement_groups << rg
      expect(org.required_containers).to match_array([sec_con1])
    end
  end

  context 'public class methods' do
    describe '::find' do
      it 'provides a custom definiton for ::find' do
        org = create :organization
        id = org.id
        slug = org.slug
        expect(Organization.find(id)).to eq org
        expect(Organization.find(slug)).to eq org
      end

      it 'can handle id as a string' do
        org = create :organization
        id = org.id
        expect(Organization.find(id.to_s)).to eq org
      end

      it 'raises an exception if unable to retrieve the organization' do
        org = create :organization
        id = org.id
        slug = org.slug
        expect(Organization.find(id)).to eq org
        expect(Organization.find(slug)).to eq org

        id = org.id + 1_000_000
        slug = "#{org.slug}-somerandomjunk"
        expect { Organization.find(id) }.to raise_error(ActiveRecord::RecordNotFound)
        expect { Organization.find(slug) }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    describe '::by_token' do
      before :each do
        @token = create :organization_token
      end

      it 'raises an exception if the organization cannot be found' do
        expect do
          Organization.by_token(rand.to_s)
        end.to raise_error(ActiveRecord::RecordNotFound)
      end

      it 'returns an organization associated with the token' do
        org = Organization.by_token(@token.value)

        expect(org).to eq @token.organization
      end
    end
  end

  context 'public instance methods' do
    it 'counts machine statuses of each type in machine_assessment_summary' do
      def create_machine(org, results)
        machine = create :machine, organization: org
        container = create :security_container
        command = create :docker_command, commandable: machine, containers: [container.name]

        results.each do |r|
          assessment = create :white_box_assessment, machine: machine, docker_command: command
          create :result, nid: 'qualys:qid1234', status: r, assessment: assessment
        end
        machine
      end

      org = create :organization
      create_machine(org, [:pass, :pass, :info, :error])  # erroring
      create_machine(org, [:pass, :info, :fail, :warn])   # failing
      create_machine(org, [:pass, :info, :warn, :warn])   # warning
      create_machine(org, [:pass, :pass, :info, :info])   # passing
      create_machine(org, [:pass, :pass, :pass])          # passing (again)
      create_machine(org, [:info, :info])                 # informing
      create_machine(org, [])                             # pending
      expect(org.machines.count).to eq 7
      expect(org.machine_assessment_summary).to match(
        erroring: 1, warning: 1, failing: 1, passing: 2, informing: 1, pending: 1
      )
    end

    it 'returns the object it inherits its RBAC from via #rbac_parent' do
      organization = build_stubbed :organization
      expect(organization.rbac_parent).to eq(organization)
    end
  end

  context 'when deleting an organization' do
    it 'deletes associated memberships' do
      org = create :organization
      user = create :user
      org.users << user
      user.add_role :organization_admin, org
      membership = Membership.where(user_id: user.id, organization_id: org.id).first

      expect(org.users).to include(user)

      org.destroy!

      expect { org.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { membership.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'deletes associated machines' do
      machine = create :machine
      org = machine.organization

      expect(org.machines).to include(machine)

      org.destroy!

      expect { org.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { machine.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'deletes organization token' do
      org = create :organization
      token = org.organization_token

      org.destroy!

      expect { org.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { token.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'deletes organization config' do
      org = create :organization
      config = org.configuration

      org.destroy!

      expect { org.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { config.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'deletes docker commands' do
      machine = create :machine
      sc = create :security_container
      org = machine.organization
      dc = create :docker_command, commandable: org, containers: [sc.name]

      org.destroy!

      expect { org.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { dc.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'deletes security container configs' do
      org = create :organization
      scc = create :security_container_config, configurable: org

      org.destroy!

      expect { org.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { scc.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  it_behaves_like 'an Ignore Scope'
  it_behaves_like 'a Protected Object' do
    let(:target) { build_stubbed :organization }
    let(:parent) { target }
  end
end
