# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: web_application_configs
#
#  id                             :integer          not null, primary key
#  auth_type                      :integer          default("unauthenticated"), not null
#  url_blacklist                  :string
#  starting_page_path             :string           default("/"), not null
#  login_form_username_field_name :string
#  login_form_password_field_name :string
#  service_id                     :integer
#  created_at                     :datetime         not null
#  updated_at                     :datetime         not null
#
# Indexes
#
#  index_web_application_configs_on_service_id  (service_id)
#
# Foreign Keys
#
#  fk_rails_048e8c2543  (service_id => services.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

require 'rails_helper'

RSpec.describe WebApplicationConfig, type: :model do
  context 'when validating' do
    subject { build(:web_application_config, auth_type: :form) }

    it { should validate_presence_of(:web_application_service) }
  end

  context 'when maintaining associations' do
    it { should belong_to(:web_application_service).with_foreign_key(:service_id) }
  end

  context 'when executing callbacks' do
    describe 'before validation' do
      it 'sets sane defaults' do
        config = WebApplicationConfig.new
        config.valid?
        expect(config.starting_page_path).to eq('/')
        expect(config.url_blacklist).to eq('')
        expect(config.auth_type).to eq('unauthenticated')
        config.auth_type = :form
        config.valid?
        expect(config.login_form_username_field_name).to eq('username')
        expect(config.login_form_password_field_name).to eq('password')
      end
    end
  end
end
