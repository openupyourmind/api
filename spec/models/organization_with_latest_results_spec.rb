# frozen_string_literal: true
require 'rails_helper'
require 'models/shared_examples/ignore_scope'

RSpec.describe OrganizationWithLatestResults do
  it 'returns the latest results for all machines in the organization via #latest_results' do
    organization = create :organization
    sectest = create :security_container
    machine1, machine2 = Array.new(2) { create :machine, organization: organization }
    org_command = create :docker_command, commandable: organization, containers: [sectest.name]
    assessment1, assessment2 = [machine1, machine2].map do |xmach|
      create :assessment, docker_command: org_command, machine: xmach, security_container: sectest
    end
    _, assessment2_result = [assessment1, assessment2].map do |xassmt|
      create :result, assessment: xassmt
    end
    machine1_command = create :docker_command, commandable: machine1, containers: [sectest.name]
    assessment3 = create :assessment, docker_command: machine1_command, machine: machine1, security_container: sectest
    assessment3_result = create :result, assessment: assessment3

    decorated_organization = described_class.new(organization)

    expected_results = Result.where(id: [assessment2_result, assessment3_result])
    expect(decorated_organization.latest_results).to match_array(expected_results)
  end
end
