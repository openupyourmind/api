# frozen_string_literal: true
require 'rails_helper'
require 'query_objects/shared_examples/results_query'
require 'query_objects/shared_examples/query_object'

RSpec.describe OrganizationResultsQuery do
  it_behaves_like 'a Query Object' do
    let(:subject) { described_class.new(double) }
  end

  it_behaves_like 'a Results Query Object' do
    let(:subject) { described_class.new(double) }
  end

  describe 'instance methods' do
    before :each do
      @organization = create :organization
    end

    it 'returns Results for all  machines in the @organization via #relation' do
      machine1 = create :machine, organization: @organization
      machine2 = create :machine, organization: @organization
      assessment1 = create :assessment, machine: machine1
      assessment2 = create :assessment, machine: machine2
      result1 = create :result, assessment: assessment1
      result2 = create :result, assessment: assessment2
      create :result # create some other result that is not tied to the organization

      query = described_class.new(Organization.where(id: @organization.id))

      expect(query.relation).to match_array(Result.where(id: [result1, result2]))
    end

    it 'returns results for a command excluding given machines via #for_command_excluding_machines' do
      machine1 = create :machine, organization: @organization
      machine2 = create :machine, organization: @organization
      assessment1 = create :assessment, machine: machine1
      assessment2 = create :assessment, machine: machine2, docker_command: assessment1.docker_command
      result1 = create :result, assessment: assessment1
      create :result, assessment: assessment2

      query = described_class.new(Organization.where(id: @organization.id))
      results = query.for_command_excluding_machines(assessment1.docker_command, machine2.id)

      expect(results).to match_array(Result.where(id: result1))
    end
  end
end
