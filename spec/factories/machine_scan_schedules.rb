# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: machine_scan_schedules
#
#  id         :integer          not null, primary key
#  machine_id :integer
#  period     :integer          default("daily"), not null
#  at         :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_machine_scan_schedules_on_machine_id  (machine_id)
#
# Foreign Keys
#
#  fk_rails_6b47f8c66a  (machine_id => machines.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

FactoryGirl.define do
  factory :machine_scan_schedule do
    at '24:24'
    period 'daily'
    machine
  end
end
