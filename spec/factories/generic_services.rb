# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: services
#
#  id              :integer          not null, primary key
#  name            :string           not null
#  description     :text
#  port            :integer          not null
#  port_type       :integer          default(0), not null
#  encryption_type :integer          default(0), not null
#  machine_id      :integer
#  type            :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_services_on_machine_id  (machine_id)
#  index_services_on_type        (type)
#
# Foreign Keys
#
#  fk_rails_b32a34656d  (machine_id => machines.id) ON DELETE => cascade
#
# rubocop:enable Metrics/LineLength

FactoryGirl.define do
  factory :generic_service do
    name 'MyString'
    description 'MyText'
    sequence(:port) { |n| (n % 65_535) + 1 }
    port_type 0 # TCP
    encryption_type 1
    machine
    type 'GenericService'
  end
end
