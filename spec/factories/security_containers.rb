# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# == Schema Information
#
# Table name: security_containers
#
#  id                     :integer          not null, primary key
#  name                   :string           not null
#  category               :integer          default("whitebox"), not null
#  prog_args              :string           not null
#  default_config         :json
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  multi_host             :boolean          default(FALSE), not null
#  test_types             :string           default([]), not null, is an Array
#  configurable           :boolean          default(FALSE), not null
#  common_service_type_id :integer
#  help_url               :string
#
# Indexes
#
#  index_security_containers_on_common_service_type_id  (common_service_type_id)
#  index_security_containers_on_name                    (name) UNIQUE
#
# Foreign Keys
#
#  fk_rails_6a0ccb7d5f  (common_service_type_id => common_service_types.id)
#
# rubocop:enable Metrics/LineLength

FactoryGirl.define do
  factory :security_container do
    name { "factory-security-container-#{SecureRandom.hex(8)}" }
    prog_args '%{target}'
    default_config { {} }
    category :blackbox
    multi_host false
    test_types ['whole_host']
    configurable false

    factory :whitebox_container do
      category :whitebox
      prog_args '%{ssh_user} %{ssh_key} %{target}'
    end
  end
end
