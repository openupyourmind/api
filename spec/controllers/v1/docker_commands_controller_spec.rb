# frozen_string_literal: true
require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::DockerCommandsController, type: :controller do
  include NoradControllerTestHelpers

  before :each do
    @org1 = create :organization
    @org2 = create :organization
    @machine = create :machine, organization: @org1
  end

  describe 'GET #index' do
    context 'Machine DockerCommand Instances' do
      it 'exposes all docker commands for a machine' do
        @_current_user.add_role :organization_admin, @org1
        commands = Array.new(2) do
          create :docker_command, commandable: @machine, containers: [create(:security_container).name]
        end
        norad_get :index, machine_id: @machine.to_param
        expect(response).to match_response_schema('docker_commands')
        expect(response.status).to eq(200)
        expect(response_body['response'].size).to eq(commands.size)
      end

      it 'exposes all docker commands for a machine even to organization readers' do
        @_current_user.add_role :organization_reader, @org1
        commands = Array.new(2) do
          create :docker_command, commandable: @machine, containers: [create(:security_container).name]
        end
        norad_get :index, machine_id: @machine.to_param
        expect(response).to match_response_schema('docker_commands')
        expect(response.status).to eq(200)
        expect(response_body['response'].size).to eq(commands.size)
      end

      context 'does not exposes all docker commands for a machine in another organization,' do
        it 'whether a reader' do
          @_current_user.add_role :organization_reader, @org2
          norad_get :index, machine_id: @machine.to_param
          expect(response.status).to eq(403)
        end

        it 'whether an admin' do
          @_current_user.add_role :organization_admin, @org2
          norad_get :index, machine_id: @machine.to_param
          expect(response.status).to eq(403)
        end
      end
    end

    context 'Organization DockerCommand Instances' do
      it 'exposes all docker commands for an organization' do
        @_current_user.add_role :organization_admin, @org1
        commands = Array.new(2) do
          create :docker_command, commandable: @org1, containers: [create(:security_container).name]
        end
        norad_get :index, organization_id: @org1.to_param
        expect(response).to match_response_schema('docker_commands')
        expect(response.status).to eq(200)
        expect(response_body['response'].size).to eq(commands.size)
      end

      it 'exposes all docker commands for an organization even to organization readers' do
        @_current_user.add_role :organization_reader, @org1
        commands = Array.new(2) do
          create :docker_command, commandable: @org1, containers: [create(:security_container).name]
        end
        norad_get :index, organization_id: @org1.to_param
        expect(response).to match_response_schema('docker_commands')
        expect(response.status).to eq(200)
        expect(response_body['response'].size).to eq(commands.size)
      end

      context 'does not exposes all docker commands for an organization in another organization,' do
        it 'whether a reader' do
          @_current_user.add_role :organization_reader, @org2
          norad_get :index, organization_id: @org1.to_param
          expect(response.status).to eq(403)
        end

        it 'whether a admin' do
          @_current_user.add_role :organization_admin, @org2
          norad_get :index, organization_id: @org1.to_param
          expect(response.status).to eq(403)
        end
      end
    end
  end

  describe 'GET #show' do
    context 'Machine DockerCommand Instances' do
      before :each do
        @docker_command = create :docker_command, commandable: @machine, containers: [create(:security_container).name]
      end

      it 'exposes an individual docker command' do
        @_current_user.add_role :organization_admin, @org1
        norad_get :show, id: @docker_command.to_param
        expect(response).to match_response_schema('docker_command')
        expect(response.status).to eq(200)
      end

      it 'exposes an individual docker command even to organization readers' do
        @_current_user.add_role :organization_reader, @org1
        norad_get :show, id: @docker_command.to_param
        expect(response).to match_response_schema('docker_command')
        expect(response.status).to eq(200)
      end

      context 'does not expose an individual docker command from another organization,' do
        it 'whether a reader' do
          @_current_user.add_role :organization_reader, @org2
          norad_get :show, id: @docker_command.to_param
          expect(response.status).to eq(403)
        end

        it 'whether an admin' do
          @_current_user.add_role :organization_admin, @org2
          norad_get :show, id: @docker_command.to_param
          expect(response.status).to eq(403)
        end
      end
    end

    context 'Organization DockerCommand Instances' do
      before :each do
        @docker_command = create :docker_command, commandable: @org1, containers: [create(:security_container).name]
      end

      it 'exposes an individual docker command' do
        @_current_user.add_role :organization_admin, @org1
        norad_get :show, id: @docker_command.to_param
        expect(response).to match_response_schema('docker_command')
        expect(response.status).to eq(200)
      end

      it 'exposes an individual docker command even to organization readers' do
        @_current_user.add_role :organization_reader, @org1
        norad_get :show, id: @docker_command.to_param
        expect(response).to match_response_schema('docker_command')
        expect(response.status).to eq(200)
      end

      context 'does not expose an individual docker command from another organization' do
        it 'whether a reader' do
          @_current_user.add_role :organization_reader, @org2
          norad_get :show, id: @docker_command.to_param
          expect(response.status).to eq(403)
        end

        it 'whether an admin' do
          @_current_user.add_role :organization_admin, @org2
          norad_get :show, id: @docker_command.to_param
          expect(response.status).to eq(403)
        end
      end
    end
  end

  describe 'DELETE #destroy' do
    context 'Machine DockerCommand Instances' do
      before :each do
        @docker_command = create :docker_command, commandable: @machine, containers: [create(:security_container).name]
      end

      context 'for organization admins' do
        before :each do
          @_current_user.add_role :organization_admin, @org1
        end

        it 'should allow removal if scan is complete' do
          @docker_command.complete!
          norad_delete :destroy, id: @docker_command.to_param
          expect(response.status).to eq(204)
        end

        it 'should render errors if scan is not complete' do
          norad_delete :destroy, id: @docker_command.to_param
          expect(response.status).to eq(422)
          expect(response_body['errors']['base']).to include('Scan must be complete before removing')
        end
      end

      context 'for organization readers' do
        before :each do
          @_current_user.add_role :organization_reader, @org1
        end

        it 'should not allow destroy' do
          norad_delete :destroy, id: @docker_command.to_param
          expect(response.status).to eq(403)
        end
      end

      context 'for other users' do
        it 'should not allow destroy for admins' do
          @_current_user.add_role :organization_admin, @org2
          norad_delete :destroy, id: @docker_command.to_param
          expect(response.status).to eq(403)
        end

        it 'should not allow destroy for readers' do
          @_current_user.add_role :organization_reader, @org2
          norad_delete :destroy, id: @docker_command.to_param
          expect(response.status).to eq(403)
        end
      end
    end

    context 'Organization DockerCommand Instances' do
      before :each do
        @docker_command = create :docker_command, commandable: @org1, containers: [create(:security_container).name]
      end

      context 'for organization admins' do
        before :each do
          @_current_user.add_role :organization_admin, @org1
        end

        it 'should allow removal if scan is complete' do
          @docker_command.complete!
          norad_delete :destroy, id: @docker_command.to_param
          expect(response.status).to eq(204)
        end

        it 'should render errors if scan is not complete' do
          norad_delete :destroy, id: @docker_command.to_param
          expect(response.status).to eq(422)
          expect(response_body['errors']['base']).to include('Scan must be complete before removing')
        end
      end

      context 'for organization readers' do
        before :each do
          @_current_user.add_role :organization_reader, @org1
        end

        it 'should not allow destroy' do
          norad_delete :destroy, id: @docker_command.to_param
          expect(response.status).to eq(403)
        end
      end

      context 'for other users' do
        it 'should not allow destroy for admins' do
          @_current_user.add_role :organization_admin, @org2
          norad_delete :destroy, id: @docker_command.to_param
          expect(response.status).to eq(403)
        end

        it 'should not allow destroy for readers' do
          @_current_user.add_role :organization_reader, @org2
          norad_delete :destroy, id: @docker_command.to_param
          expect(response.status).to eq(403)
        end
      end
    end
  end

  describe 'POST #create' do
    context 'Machine DockerCommand Instances' do
      context 'with valid params' do
        def create_command
          attrs = { containers: [create(:security_container).name] }
          norad_post :create, machine_id: @machine.id, docker_command: attrs
        end

        it 'creates an instance of a docker command' do
          @_current_user.add_role :organization_admin, @org1
          expect { create_command }.to change(DockerCommand, :count).by(1)
          expect(response.status).to eq(200)
        end

        it 'try to create an instance as a organization reader and fail' do
          @_current_user.add_role :organization_reader, @org1
          expect { create_command }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'try to create an instance as a organization reader from another org and fail' do
          @_current_user.add_role :organization_reader, @org2
          expect { create_command }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'try to create an instance as in another organization' do
          @_current_user.add_role :organization_admin, @org2
          expect { create_command }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end
      end

      context 'with invalid params' do
        it 'responds with an error message to admin' do
          @_current_user.add_role :organization_admin, @org1
          norad_post :create, machine_id: @machine.id, docker_command: { containers: ['blah'] }
          expect(response.status).to eq(422)
        end

        it 'responds with a 403 to reader' do
          @_current_user.add_role :organization_reader, @org1
          norad_post :create, machine_id: @machine.id, docker_command: { containers: ['blah'] }
          expect(response.status).to eq(403)
        end
      end
    end

    context 'Organization DockerCommand Instances' do
      context 'with valid params' do
        def create_command
          attrs = { containers: [create(:security_container).name] }
          norad_post :create, organization_id: @org1.to_param, docker_command: attrs
        end

        it 'creates an instance of a docker command' do
          @_current_user.add_role :organization_admin, @org1
          expect { create_command }.to change(DockerCommand, :count).by(1)
          expect(response.status).to eq(200)
        end

        it 'try to create an instance as a organization reader and fail' do
          @_current_user.add_role :organization_reader, @org1
          expect { create_command }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'try to create an instance as in another organization' do
          @_current_user.add_role :organization_admin, @org2
          expect { create_command }.to change(DockerCommand, :count).by(0)
          expect(response.status).to eq(403)
        end
      end

      context 'with invalid params' do
        it 'responds with an error message to an admin' do
          @_current_user.add_role :organization_admin, @org1
          norad_post :create, machine_id: @machine.id, docker_command: { containers: ['blah'] }
          expect(response.status).to eq(422)
        end

        it 'responds with a 403 to a reader' do
          @_current_user.add_role :organization_reader, @org1
          norad_post :create, machine_id: @machine.id, docker_command: { containers: ['blah'] }
          expect(response.status).to eq(403)
        end
      end
    end
  end
end
