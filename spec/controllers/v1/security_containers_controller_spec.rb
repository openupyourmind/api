# frozen_string_literal: true
require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::SecurityContainersController, type: :controller do
  include NoradControllerTestHelpers

  before :all do
    @standard_user = create :user
  end

  describe 'GET #index' do
    it 'exposes all security containers' do
      2.times do
        create :security_container
      end
      norad_get :index, {}
      expect(response).to match_response_schema('security_containers')
    end
  end

  describe 'GET #show' do
    it 'exposes an individual container' do
      container = create :security_container
      norad_get :show, id: container.to_param
      expect(response).to match_response_schema('security_container')
    end
  end
end
