# frozen_string_literal: true
require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::ResultIgnoreRulesController, type: :controller do
  include NoradControllerTestHelpers

  before :each do
    @org1 = create :organization
    @org2 = create :organization
    @machine = create :machine, organization: @org1
  end

  describe 'GET #index' do
    let(:rule_count) { 2 }

    # rubocop:disable Metrics/AbcSize
    def expect_success
      expect(response).to match_response_schema('result_ignore_rules')
      expect(response.status).to eq(200)
      expect(response_body['response'].size).to eq(rule_count)
    end
    # rubocop:enable Metrics/AbcSize

    context 'Machine ResultIgnoreRule instances' do
      before(:each) { rule_count.times { create(:result_ignore_rule, ignore_scope: @machine) } }

      it 'exposes all result ignore rules to org admins' do
        @_current_user.add_role :organization_admin, @org1
        norad_get :index, machine_id: @machine.to_param
        expect_success
      end

      it 'exposes all result ignore rules to org readers' do
        @_current_user.add_role :organization_reader, @org1
        norad_get :index, machine_id: @machine.to_param
        expect_success
      end

      context 'prevents exposure of result ignore rules for machine in other org' do
        it 'whether a reader' do
          @_current_user.add_role :organization_reader, @org2
          norad_get :index, machine_id: @machine.to_param
          expect(response.status).to eq(403)
        end

        it 'whether an admin' do
          @_current_user.add_role :organization_admin, @org2
          norad_get :index, machine_id: @machine.to_param
          expect(response.status).to eq(403)
        end
      end
    end

    context 'Organization ResultIgnoreRule instances' do
      before(:each) { rule_count.times { create(:result_ignore_rule, ignore_scope: @org1) } }

      it 'exposes all result ignore rules to org admins' do
        @_current_user.add_role :organization_admin, @org1
        norad_get :index, organization_id: @org1.to_param
        expect_success
      end

      it 'exposes all result ignore rules to org readers' do
        @_current_user.add_role :organization_reader, @org1
        norad_get :index, organization_id: @org1.to_param
        expect_success
      end

      context 'prevents exposure of result ignore rules for machine in other org' do
        it 'whether a reader' do
          @_current_user.add_role :organization_reader, @org2
          norad_get :index, organization_id: @org1.to_param
          expect(response.status).to eq(403)
        end

        it 'whether an admin' do
          @_current_user.add_role :organization_admin, @org2
          norad_get :index, organization_id: @org1.to_param
          expect(response.status).to eq(403)
        end
      end
    end
  end

  describe 'GET #show' do
    def expect_success
      expect(response).to match_response_schema('result_ignore_rule')
      expect(response.status).to eq(200)
    end

    context 'Machine ResultIgnoreRule instances' do
      before(:each) { @rule = create :result_ignore_rule, ignore_scope: @machine }

      it 'exposes an individual ignore rule' do
        @_current_user.add_role :organization_admin, @org1
        norad_get :show, id: @rule.to_param
        expect_success
      end

      it 'exposes an individual ignore rule even to organization readers' do
        @_current_user.add_role :organization_reader, @org1
        norad_get :show, id: @rule.to_param
        expect_success
      end

      context 'does not expose an individual ignore rule from another organization,' do
        it 'whether a reader' do
          @_current_user.add_role :organization_reader, @org2
          norad_get :show, id: @rule.to_param
          expect(response.status).to eq(403)
        end

        it 'whether an admin' do
          @_current_user.add_role :organization_admin, @org2
          norad_get :show, id: @rule.to_param
          expect(response.status).to eq(403)
        end
      end
    end

    context 'Organization ResultIgnoreRule instances' do
      before(:each) { @rule = create :result_ignore_rule, ignore_scope: @org1 }

      it 'exposes an individual ignore rule' do
        @_current_user.add_role :organization_admin, @org1
        norad_get :show, id: @rule.to_param
        expect_success
      end

      it 'exposes an individual ignore rule even to organization readers' do
        @_current_user.add_role :organization_reader, @org1
        norad_get :show, id: @rule.to_param
        expect_success
      end

      context 'does not expose an individual ignore rule from another organization' do
        it 'whether a reader' do
          @_current_user.add_role :organization_reader, @org2
          norad_get :show, id: @rule.to_param
          expect(response.status).to eq(403)
        end

        it 'whether an admin' do
          @_current_user.add_role :organization_admin, @org2
          norad_get :show, id: @rule.to_param
          expect(response.status).to eq(403)
        end
      end
    end
  end

  describe 'POST #create' do
    def create_rule
      atts = attributes_for(:result_ignore_rule, ignore_scope: @machine, created_by: @_current_user.uid)
      norad_post :create, machine_id: @machine.to_param, result_ignore_rule: atts
    end

    context 'Machine ResultIgnoreRule instances' do
      context 'with valid params' do
        it 'creates an instance of an ignore rule' do
          @_current_user.add_role :organization_admin, @org1
          expect { create_rule }.to change(ResultIgnoreRule, :count).by(1)
          expect(response.status).to eq(200)
          expect(response_body['response']['created_by']).to eq(@_current_user.uid)
        end

        it 'try to create an instance as a organization reader and fail' do
          @_current_user.add_role :organization_reader, @org1
          expect { create_rule }.to change(ResultIgnoreRule, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'try to create an instance as a organization reader from another org and fail' do
          @_current_user.add_role :organization_reader, @org2
          expect { create_rule }.to change(ResultIgnoreRule, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'try to create an instance as in another organization' do
          @_current_user.add_role :organization_admin, @org2
          expect { create_rule }.to change(ResultIgnoreRule, :count).by(0)
          expect(response.status).to eq(403)
        end
      end

      context 'with invalid params' do
        it 'responds with an error message to admin' do
          @_current_user.add_role :organization_admin, @org1
          norad_post :create, machine_id: @machine.to_param, result_ignore_rule: { signature: 'invalid' }
          expect(response.status).to eq(422)
        end

        it 'responds with a 403 to reader' do
          @_current_user.add_role :organization_reader, @org1
          norad_post :create, machine_id: @machine.to_param, result_ignore_rule: { signature: 'invalid' }
          expect(response.status).to eq(403)
        end
      end
    end

    context 'Organization ResultIgnoreRule instances' do
      context 'with valid params' do
        def create_rule
          attrs = attributes_for(:result_ignore_rule, ignore_scope: @org1, created_by: @_current_user.uid)
          norad_post :create, organization_id: @org1.to_param, result_ignore_rule: attrs
        end

        it 'creates an instance of a result ignore rule' do
          @_current_user.add_role :organization_admin, @org1
          expect { create_rule }.to change(ResultIgnoreRule, :count).by(1)
          expect(response.status).to eq(200)
          expect(response_body['response']['created_by']).to eq(@_current_user.uid)
        end

        it 'try to create an instance as a organization reader and fail' do
          @_current_user.add_role :organization_reader, @org1
          expect { create_rule }.to change(ResultIgnoreRule, :count).by(0)
          expect(response.status).to eq(403)
        end

        it 'try to create an instance as in another organization' do
          @_current_user.add_role :organization_admin, @org2
          expect { create_rule }.to change(ResultIgnoreRule, :count).by(0)
          expect(response.status).to eq(403)
        end
      end

      context 'with invalid params' do
        it 'responds with an error message to an admin' do
          @_current_user.add_role :organization_admin, @org1
          norad_post :create, machine_id: @machine.id, result_ignore_rule: { signature: 'invalid' }
          expect(response.status).to eq(422)
        end

        it 'responds with a 403 to a reader' do
          @_current_user.add_role :organization_reader, @org1
          norad_post :create, machine_id: @machine.id, result_ignore_rule: { signature: 'invalid' }
          expect(response.status).to eq(403)
        end
      end
    end
  end

  describe 'DELETE #destroy' do
    context 'Machine ResultIgnoreRule Instances' do
      before(:each) { @rule = create :result_ignore_rule, ignore_scope: @machine }

      it 'should allow removal for admins' do
        @_current_user.add_role :organization_admin, @org1
        norad_delete :destroy, id: @rule.to_param
        expect(response.status).to eq(204)
      end

      it 'prevents removal for readers' do
        @_current_user.add_role :organization_reader, @org1
        norad_delete :destroy, id: @rule.to_param
        expect(response.status).to eq(403)
      end

      context 'for other users' do
        it 'prevents destroy for admins' do
          @_current_user.add_role :organization_admin, @org2
          norad_delete :destroy, id: @rule.to_param
          expect(response.status).to eq(403)
        end

        it 'prevents destroy for readers' do
          @_current_user.add_role :organization_reader, @org2
          norad_delete :destroy, id: @rule.to_param
          expect(response.status).to eq(403)
        end
      end
    end

    context 'Organization ResultIgnoreRule Instances' do
      before(:each) { @rule = create :result_ignore_rule, ignore_scope: @org1 }

      it 'should allow removal for admins' do
        @_current_user.add_role :organization_admin, @org1
        norad_delete :destroy, id: @rule.to_param
        expect(response.status).to eq(204)
      end

      it 'prevents removal for readers' do
        @_current_user.add_role :organization_reader, @org1
        norad_delete :destroy, id: @rule.to_param
        expect(response.status).to eq(403)
      end

      context 'for other users' do
        it 'prevents destroy for admins' do
          @_current_user.add_role :organization_admin, @org2
          norad_delete :destroy, id: @rule.to_param
          expect(response.status).to eq(403)
        end

        it 'prevents destroy for readers' do
          @_current_user.add_role :organization_reader, @org2
          norad_delete :destroy, id: @rule.to_param
          expect(response.status).to eq(403)
        end
      end
    end
  end
end
