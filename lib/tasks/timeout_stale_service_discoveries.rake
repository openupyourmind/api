# frozen_string_literal: true
desc 'If a service discovery has been in progress for longer than 48 hours, move it to a failed state'
task timeout_stale_service_discoveries: :environment do
  ServiceDiscovery.in_progress.where('updated_at <= ?', 48.hours.ago).each do |sd|
    sd.error_message = 'This task has not finished in at least 48 hours. Please contact Norad support.'
    sd.fail!
  end
end
