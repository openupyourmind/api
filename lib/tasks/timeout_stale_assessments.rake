# frozen_string_literal: true
desc 'If an assessment has been in progress for longer than 48 hours, move it to a completed state'
task timeout_stale_assessments: :environment do
  (WhiteBoxAssessment.stale + BlackBoxAssessment.stale).each(&:timeout!)
end
