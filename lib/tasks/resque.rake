# frozen_string_literal: true
unless Rails.env.test?
  require 'resque/tasks'

  namespace :resque do
    task 'resque:setup' => :environment do
      require 'resque'
    end
  end
end

if Rails.env.production?
  require 'resque/tasks'
  require 'resque/scheduler/tasks'

  namespace :resque do
    task setup_schedule: :setup do
      require 'resque-scheduler'
      Resque::Scheduler.dynamic = true
      Resque::Scheduler.configure do |c|
        c.quiet = false
        c.verbose = true
        c.logfile = "#{Rails.root}/log/resque_scheduler_#{Rails.env}.log"
        c.logformat = 'json'
      end
    end

    task scheduler: :setup_schedule
  end
end
