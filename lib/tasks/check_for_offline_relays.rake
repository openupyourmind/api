# frozen_string_literal: true
desc 'If a Relay has not sent a heartbeat in an hour, change its state to offline'
task check_for_offline_relays: :environment do
  DockerRelay.online.where('last_heartbeat <= ?', 1.hour.ago).each(&:go_offline!)
end
