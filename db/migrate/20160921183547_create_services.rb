class CreateServices < ActiveRecord::Migration[4.2]
  def change
    create_table :services do |t|
      t.string :name, null: false
      t.text :description
      t.integer :port, null: false
      t.integer :port_type, default: 0, null: false
      t.integer :encryption_type, default: 0, null: false
      t.references :machine, index: true
      t.string :type, index: true, null: false

      t.timestamps null: false
    end
    add_foreign_key :services, :machines, on_delete: :cascade
  end
end
