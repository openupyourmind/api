class DropSshConfigs < ActiveRecord::Migration[4.2]
  def up
    drop_table :ssh_configs
  end

  def down
    create_table :ssh_configs do |t|
      t.references :machine, index: true, unique: true
      t.string :username_encrypted
      t.text :key_encrypted

      t.timestamps null: false
    end
    add_foreign_key :ssh_configs, :machines, on_delete: :cascade
  end
end
