class CreateAgentCommands < ActiveRecord::Migration[4.2]
  def change
    create_table :agent_commands do |t|
      t.references :commandable, polymorphic: true, index: true
      t.text :error_details
      t.integer :state, default: 0, null: false
      t.json :statement, null: false

      t.timestamps null: false
    end
  end
end
