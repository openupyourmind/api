class DockerRelayDefaultStateOnline < ActiveRecord::Migration[4.2]
  def change
    # Change state to "online"	  
    change_column_default :docker_relays, :state, 1
  end
end
