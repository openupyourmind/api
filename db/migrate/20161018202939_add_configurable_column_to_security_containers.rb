class AddConfigurableColumnToSecurityContainers < ActiveRecord::Migration[4.2]
  def change
    add_column :security_containers, :configurable, :boolean, default: false, null: false
  end
end
