class CreateProvisions < ActiveRecord::Migration[4.2]
  def change
    create_table :provisions do |t|
      t.references :security_container, index: true, null: false
      t.references :requirement, index: true, null: false

      t.timestamps null: false
    end
    add_foreign_key :provisions, :security_containers, on_delete: :cascade
    add_foreign_key :provisions, :requirements, on_delete: :cascade
  end
end
