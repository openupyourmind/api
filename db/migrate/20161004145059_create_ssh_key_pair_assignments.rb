class CreateSshKeyPairAssignments < ActiveRecord::Migration[4.2]
  def change
    create_table :ssh_key_pair_assignments do |t|
      t.references :machine, null: false
      t.references :ssh_key_pair, index: true, null: false
      t.timestamps null: false
      t.index :machine_id, unique: true
    end
    add_foreign_key :ssh_key_pair_assignments, :machines, on_delete: :cascade
    add_foreign_key :ssh_key_pair_assignments, :ssh_key_pairs, on_delete: :cascade
  end
end
