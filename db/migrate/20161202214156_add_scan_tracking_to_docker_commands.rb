class AddScanTrackingToDockerCommands < ActiveRecord::Migration[5.0]
  def change
    add_column :docker_commands, :assessments_in_flight, :integer, default: 0
    add_column :docker_commands, :started_at, :datetime
    add_column :docker_commands, :finished_at, :datetime
  end
end
