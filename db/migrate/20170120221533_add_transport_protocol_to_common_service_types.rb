class AddTransportProtocolToCommonServiceTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :common_service_types, :transport_protocol, :integer, default: 0, null: false
  end
end
