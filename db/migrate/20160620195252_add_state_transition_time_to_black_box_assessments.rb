class AddStateTransitionTimeToBlackBoxAssessments < ActiveRecord::Migration[4.2]
  class Assessment < ApplicationRecord
    self.abstract_class = true
  end

  class BlackBoxAssessment < Assessment
    self.table_name = 'black_box_assessments'
  end

  def up
    add_column :black_box_assessments, :state_transition_time, :timestamp
    BlackBoxAssessment.reset_column_information
    BlackBoxAssessment.all.each do |assessment|
      assessment.update_column(:state_transition_time, assessment.updated_at)
    end
    change_column_null :black_box_assessments, :state_transition_time, false
  end

  def down
    remove_column :black_box_assessments, :state_transition_time
  end
end
