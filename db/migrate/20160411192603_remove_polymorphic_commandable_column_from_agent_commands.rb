class RemovePolymorphicCommandableColumnFromAgentCommands < ActiveRecord::Migration[4.2]
  class AgentCommand < ActiveRecord::Base
    belongs_to :commandable, polymorphic: true
    belongs_to :machine
    belongs_to :organization
  end

  def up
    change_table :agent_commands do |t|
      t.references :machine, index: true
      t.references :organization, index: true
    end
    add_foreign_key :agent_commands, :machines, on_delete: :cascade
    add_foreign_key :agent_commands, :organizations, on_delete: :cascade
    AgentCommand.reset_column_information

    AgentCommand.all.each do |ac|
      if ac.commandable_type == 'Organization'
        ac.organization_id = ac.commandable_id
      else
        ac.machine_id = ac.commandable_id
      end
      ac.save!
    end

    change_table :agent_commands do |t|
      t.remove :commandable_type
      t.remove :commandable_id
    end
  end

  def down
    change_table :agent_commands do |t|
      t.references :commandable, polymorphic: true, index: true
    end
    AgentCommand.reset_column_information

    AgentCommand.all.each do |ac|
      if ac.machine
        ac.commandable = ac.machine
      elsif ac.organization
        ac.commandable = ac.organization
      end
      ac.save!
    end

    change_table :agent_commands do |t|
      t.remove :machine_id
      t.remove :organization_id
    end
  end
end
