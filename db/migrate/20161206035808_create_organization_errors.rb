class CreateOrganizationErrors < ActiveRecord::Migration[5.0]
  def change
    create_table :organization_errors do |t|
      t.references :organization
      t.string :type
      t.string :message

      t.timestamps
    end
    add_foreign_key :organization_errors, :organizations, on_delete: :cascade
  end
end
