class MoveSshConfigToKeypairs < ActiveRecord::Migration[4.2]
  class SshConfig < ActiveRecord::Base
    # Vault Stuff
    include Vault::EncryptedModel
    vault_attribute :key
    vault_attribute :username

    belongs_to :machine
  end

  def up
    create_keypair = -> (org, config, name) do
      keypair = SshKeyPair.new(username: config.username, key: config.key, name: name)
      org.ssh_key_pairs << keypair
      config.machine.ssh_key_pair = keypair
    end

    SshConfig.all.each do |config|
      org = config.machine.organization
      pair_count = org.ssh_key_pairs.count
      keypair = org.ssh_key_pairs.to_a.detect do |k|
        k.username == config.username && k.key == config.key
      end
      keypair.nil? ? create_keypair.call(org, config, "key #{pair_count + 1}") : config.machine.ssh_key_pair = keypair
    end
  end

  def down
    SshKeyPair.all.each do |keypair|
      keypair.machines.each do |machine|
        SshConfig.create(key: keypair.key, username: keypair.username, machine_id: machine.id)
      end
    end
    SshKeyPair.all.destroy_all
  end
end
