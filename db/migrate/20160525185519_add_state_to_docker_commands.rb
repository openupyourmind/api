class AddStateToDockerCommands < ActiveRecord::Migration[4.2]
  def up
    add_column :docker_commands, :state, :integer, default: 0, null: false
    DockerCommand.reset_column_information
    DockerCommand.all.each do |dc|
      dc.update_column(:state, DockerCommand::COMPLETE_STATE_NUM)
    end
  end

  def down
    remove_column :docker_commands, :state
  end
end
