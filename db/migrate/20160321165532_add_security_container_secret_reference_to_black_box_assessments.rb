class AddSecurityContainerSecretReferenceToBlackBoxAssessments < ActiveRecord::Migration[4.2]
  def change
    add_reference :black_box_assessments, :security_container_secret, index: true
    add_foreign_key :black_box_assessments, :security_container_secrets, on_delete: :cascade
  end
end
