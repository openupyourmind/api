sc = SecurityContainer.where(name: 'norad-registry.cisco.com:5000/sslyze-heartbleed:0.0.1').first_or_initialize
sc.prog_args = '%{target}:%{port}'
sc.category = :blackbox
sc.test_types = ["ssl_crypto"]
sc.help_url = 'https://norad.gitlab.io/docs/#sslyze-heartbleed'
sc.save!

sc = SecurityContainer.where(name: 'norad-registry.cisco.com:5000/nmap-ssl-dh-param:0.0.1').first_or_initialize
sc.prog_args = '-p %{port} %{target}'
sc.category = :blackbox
sc.test_types = ["ssl_crypto"]
sc.help_url = 'https://norad.gitlab.io/docs/#nmap-ssl-dh-param'
sc.save!

sc = SecurityContainer.where(name: 'norad-registry.cisco.com:5000/vuls:0.0.1').first_or_initialize
sc.prog_args = '%{target} %{ssh_user} %{port} %{ssh_key}'
sc.category = :whitebox
sc.test_types = ["authenticated"]
sc.help_url = 'https://norad.gitlab.io/docs/#vuls'
sc.save!

sc = SecurityContainer.where(name: 'norad-registry.cisco.com:5000/zap-passive:0.0.1').first_or_initialize
sc.prog_args = 'scan -s passive -a %{web_service_auth_type} -t %{web_service_protocol}://%{target} -l %{web_service_starting_page_path} -U %{web_service_login_form_username_field_name} -P %{web_service_login_form_password_field_name} -u %{service_username} -p %{service_password}'
sc.category = :blackbox
sc.test_types = ["web_application"]
sc.help_url = 'https://norad.gitlab.io/docs/#zap-passive'
sc.save!

sc = SecurityContainer.where(name: 'norad-registry.cisco.com:5000/hydra-mysql:0.0.1').first_or_initialize
sc.prog_args = '%{target}:%{port}'
sc.category = :blackbox
sc.test_types = ["brute_force"]
sc.common_service_type = CommonServiceType.find_by(name: 'mysql')
sc.help_url = 'https://norad.cisco.com/docs/#hydra-mysql'
sc.save!

sc = SecurityContainer.where(name: 'norad-registry.cisco.com:5000/hydra-ftp:0.0.1').first_or_initialize
sc.prog_args = '%{target}:%{port}'
sc.category = :blackbox
sc.test_types = ["brute_force"]
sc.common_service_type = CommonServiceType.find_by(name: 'ftp')
sc.help_url = 'https://norad.gitlab.io/docs/#hydra-ftp'
sc.save!

sc = SecurityContainer.where(name: 'norad-registry.cisco.com:5000/nmap-snmp-bruteforce:0.0.1').first_or_initialize
sc.prog_args = '-p %{port} %{target}'
sc.category = :blackbox
sc.test_types = ["brute_force"]
sc.common_service_type = CommonServiceType.find_by(name: 'snmp')
sc.help_url = 'https://norad.gitlab.io/docs/#nmap-snmp-brute-force'
sc.save!

sc = SecurityContainer.where(name: 'norad-registry.cisco.com:5000/hydra-ssh-bruteforce:0.0.1').first_or_initialize
sc.prog_args = '%{target}:%{port}'
sc.category = :blackbox
sc.test_types = ["brute_force"]
sc.common_service_type = CommonServiceType.find_by(name: 'ssh')
sc.help_url = 'https://norad.gitlab.io/docs/#hydra-brute-force-ssh'
sc.save!

sc = SecurityContainer.where(name: 'norad-registry.cisco.com:5000/hydra-postgres:0.0.1').first_or_initialize
sc.prog_args = '%{target}:%{port}'
sc.category = :blackbox
sc.test_types = ["brute_force"]
sc.common_service_type = CommonServiceType.find_by(name: 'postgresql')
sc.help_url = 'https://norad.gitlab.io/docs/#hydra-postgres'
sc.save!
