#!/usr/bin/env ruby
# frozen_string_literal: true

# A utility script for loading security container data from a Gitlab build artifact
#
# Execute this script with bin/rails runner in the project directory. This script does its own
# command line processing so as not to introduce a new dependency (e.g. Thor) to the project.

require 'zip'
require 'net/http'

module SecurityContainerLoader
  ARCHIVE_FILE_PATH = "#{Rails.root}/tmp/security_container_build_artifact.zip"

  class << self
    def run(args)
      ensure_running_in_rails_app
      artifact_url = parse_argv(args)
      GitlabArtifact.new(artifact_url).seed_containers
    end

    private

    def parse_argv(args)
      return use_environment_variable if args.empty?
      artifact_url = ARGV.shift
      exit_with_usage unless artifact_url =~ %r{\Ahttps?://\S}
      artifact_url
    end

    def use_environment_variable
      ENV['SECURITY_CONTAINERS_SEED_URL'] || exit_with_usage
    end

    def ensure_running_in_rails_app
      exit_with_usage unless defined?(Rails)
    end

    def exit_with_usage
      print_usage
      exit(1)
    end

    def print_usage
      puts "rails runner #{__FILE__} <gitlab_build_artifact_url>"
    end
  end

  def send_request(url)
    uri = URI(url)
    Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') do |http|
      request = Net::HTTP::Get.new uri
      http.request request do |resp|
        return(yield resp)
      end
    end
  end

  def exit_with_message(msg)
    puts msg
    exit(1)
  end

  class GitlabArtifact
    attr_reader :artifact_url

    include SecurityContainerLoader

    def initialize(artifact_url)
      @artifact_url = artifact_url
    end

    def seed_containers
      download_artifact artifact_url
      seed_data = extract_containers_file
      exit_with_message('Unable to find seed data in artifact') unless seed_data
      load_seed_data(seed_data)
    end

    private

    def get_artifact_url_from_redirect(url)
      send_request(url) do |resp|
        resp.is_a?(Net::HTTPRedirection) ? resp['location'] : url
      end
    end

    def download_and_save(url)
      send_request(url) do |resp|
        open ARCHIVE_FILE_PATH, 'wb' do |io|
          resp.read_body do |chunk|
            io.write chunk
          end
        end
      end
    end

    def extract_containers_file
      # Overwrite existing containers file, if it exists
      Zip.on_exists_proc = true

      seed_file = nil
      Zip::File.open(ARCHIVE_FILE_PATH) do |zip_file|
        puts 'Extracting seed file from artifact...'
        seed_file = zip_file.detect { |entry| entry.name == 'containers.rb' }
      end
      seed_file
    end

    # rubocop:disable Security/Eval
    # We disable the eval check because the whole purpose of this script is to execute the Ruby code
    # found in the artifact. This script is only executed via the command line.
    def load_seed_data(seed_data)
      eval(seed_data.get_input_stream.read)
    end
    # rubocop:enable Security/Eval

    def download_artifact(url)
      puts "Downloading from #{url}..."
      download_and_save(get_artifact_url_from_redirect(url))
      puts "Done! File artifact written to #{ARCHIVE_FILE_PATH}"
    end
  end
end

SecurityContainerLoader.run ARGV
