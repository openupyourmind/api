# frozen_string_literal: true
# set path to application
app_dir = File.expand_path('../..', __FILE__)
working_directory app_dir

# Set unicorn options
user 'cloud-user'
worker_processes 2
preload_app true
timeout 30

# Set up socket location
listen '/var/run/norad/api/api.sock', backlog: 64

# Logging
stderr_path '/var/log/norad/api/unicorn.stderr.log'
stdout_path '/var/log/norad/api/unicorn.stdout.log'

# Set master PID location
pid '/var/run/norad/api/api.pid'
